﻿using UnityEngine;
using System.Collections;

public class SharkScript : UnitScript {
	
	private Vector3 sharkTarget;
	private Transform playerPos;

	// Use this for initialization
	void Start () {
		playerPos = GameObject.FindGameObjectWithTag("Player").transform;
		sharkTarget = new Vector3 (playerPos.position.x*40, transform.position.y, playerPos.position.z*40);
	}
	
	// Update is called once per frame
	void Update () {
		transform.forward = Vector3.Lerp (transform.position, sharkTarget, Time.deltaTime);
	}
}
