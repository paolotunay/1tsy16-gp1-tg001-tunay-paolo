﻿using UnityEngine;
using System.Collections;

public class EnemyshipScript : UnitScript {

	private Vector3 enemyShipPos;
	private Transform playerPos;

	// Use this for initialization
	void Start () {
		playerPos = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		enemyShipPos = new Vector3 (playerPos.position.x - 3, transform.position.y, playerPos.position.z - 3);
		transform.position = Vector3.Lerp(transform.position, enemyShipPos, Time.deltaTime / 2);
	}
}
