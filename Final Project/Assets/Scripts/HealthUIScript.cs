﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthUIScript : MonoBehaviour {

	public Sprite[] HealthSprites;
	public Image PlayerLifeUI;
	private PlayerScript player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
	}
	
	// Update is called once per frame
	void Update () {
		PlayerLifeUI.sprite = HealthSprites [player.health];
	}
}
