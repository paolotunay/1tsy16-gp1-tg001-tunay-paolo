﻿using UnityEngine;
using System.Collections;

public class PlayerScript : UnitScript {

	public float rotationSpeed = 450;
	public int health = 3;

	//SHOOTING MECHANIC 
	private float shootRate;
	private float elapsedTime;

	private Quaternion targetRotation;


	// Use this for initialization
	void Start () {
		shootRate = .5f;
	}
	
	// Update is called once per frame
	void Update () {

		//Update the time
		elapsedTime += Time.deltaTime;

		Vector3 mousePos = Input.mousePosition;
		Vector3 transPos = new Vector3 (transform.position.x, 0, transform.position.z);
		mousePos = Camera.main.ScreenToWorldPoint (new Vector3 (mousePos.x, mousePos.y, Camera.main.transform.position.y - transform.position.y));

		//move player
		targetRotation = Quaternion.LookRotation (mousePos - transPos);
		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);
		transform.position = Vector3.Lerp (transform.position, new Vector3 (mousePos.x, 0, mousePos.z), 30 * Time.deltaTime /20);

		//draw Ray from Player to Cursor
		Debug.DrawRay (transform.position, mousePos - transPos, Color.yellow);
		UpdateWeapon();

		if (Input.GetMouseButtonDown (0)) {
			health--;
			Debug.Log ("health subtracted by 1");
		}
	}

	void UpdateWeapon(){

		if (elapsedTime >= shootRate){
				//Reset the time
				elapsedTime = 0.0f;

				//Instantiate(Bullet, transform.position, transform.rotation);
				//Debug.DrawLine(transform.position, transform.forward, Color.green);
			}
	}

}
 