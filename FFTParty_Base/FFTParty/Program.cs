﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Console.WriteLine("Welcome to Ivalice!");

            PlayerRoster playerRoster = new PlayerRoster();
            playerRoster.Gil = 2000;

            string input = "";
            while (input != "n")
            {
                string recruitInput = "";
                while (recruitInput != "n")
                {
                    Console.Write("Would you like to visit more cities to recruit? (y/n): ");
                    recruitInput = Console.ReadLine();
                    if (recruitInput == "n")
                        break;

                    /// QUIZ ITEM
                    /// Print all available cities in the game
                    /// You can get them from GameDatabase.GetCity(int id)
                    /// There are only 6 valid cities, w/ id numbers from 1-6
                    /// Hint: For loop
                    // Print all cities
                    for (int x = 1; x < 7; x++)
                    {
                        Console.WriteLine("(" + x + ") " + GameDatabase.GetCity(x).Name);
                    }

                    // Ask user w/c city he would like to visit
                    Console.Write("Please enter the city you would like to visit: ");
                    int cityId = Convert.ToInt32(Console.ReadLine());

                    /// QUIZ ITEM
                    /// Get the city from the database by GameDatabase.GetCity(int id)
                    City city = new City(GameDatabase.GetCity(cityId).Name);
                    city = GameDatabase.GetCity(cityId);

                    /// QUIZ ITEM
                    /// Enter the city by calling city.Recruit()
                    /// You must pass in the player's party
                    city.Recruit(playerRoster);

                    Console.WriteLine();
                }

                playerRoster.PrintRoster();

                Console.WriteLine("-----------");
                Console.WriteLine("Your party has travelled Ivalice and encountered monsters!");

                List<Monster> monsters = new List<Monster>();
                int monsterCount = random.Next(1, 7);
                for (int i = 0; i <= monsterCount; i++)
                {
                    /// QUIZ ITEM
                    /// Randomize a Monster ID from 1-10
                    /// and add it to the monsters list
                    /// You can get an Monster by calling
                    /// GameDatabase.GetMonster(int id)
                    int monsterId = random.Next(1, 10);
                    monsters.Add(GameDatabase.GetMonster(monsterId));
                }

                // Fight Monsters
                foreach (Monster monster in monsters)
                {
                    playerRoster.EncounterMonster(monster);
                }

                Console.WriteLine("Encounter over.");
                // Display player's earnings
                playerRoster.ComputeEarnings();

                Console.Write("Would you like to go on another adventure? (y/n): ");
                input = Console.ReadLine();
            }
            Console.Read();
        }
    }
}
