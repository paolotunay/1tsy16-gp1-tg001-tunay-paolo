﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    public class City
    {
        public string Name { get; private set; }
        public List<Character> RecruitableCharacters = new List<Character>();

        public City(string name)
        {
            Name = name;
        }

        public void PrintRecruitableCharacters()
        {
            Console.WriteLine("Welcome to the " + Name);
            Console.WriteLine("Recruitable Characters: ");
            for (int i = 0; i < RecruitableCharacters.Count; i++)
            {
                Character character = RecruitableCharacters[i];
                Console.Write(i+1 + ")");
                character.PrintRecruitmentDetails();
            }
        }

        public void Recruit(PlayerRoster player)
        {
            while (true)
            {
                PrintRecruitableCharacters();
                Console.WriteLine("-------------");
                Console.WriteLine();
                Console.Write("Please select a character: ");
                int characterSelection = Convert.ToInt32(Console.ReadLine());
                characterSelection -= 1; // To make it 0 based
                if (characterSelection >= RecruitableCharacters.Count || characterSelection < 0)
                {
                    Console.WriteLine("Invalid selection");
                    continue;
                }
                Character character = RecruitableCharacters[characterSelection];
                Character recruitCharacter = GameDatabase.GetCharacter(character.Id);

                Console.WriteLine();
                Console.WriteLine("Successfully recruited: " + recruitCharacter.JobName);
                Console.Write("Enter a name for this character: ");
                string name = Console.ReadLine();
                recruitCharacter.Name = name;

                player.Characters.Add(recruitCharacter);

                string input = "";
                Console.Write("Would you like to recruit more? (y/n): ");
                input = Console.ReadLine();
                if (input != "y")
                    break;
            }
            
        }
    }
}
