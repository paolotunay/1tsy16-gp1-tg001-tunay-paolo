﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Map
    {
        // Constructor
        public Map ()
        {
            PlayerXPos = 0;
            PlayerYPos = 0;
        }

        public int PlayerXPos { get; private set; }
        public int PlayerYPos { get; private set; }
        public void MovePlayer()
        {
            int selection = 1;
            Console.WriteLine("1) Go North,    2) Go East,     3) South,      4) West");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:
                    PlayerYPos++;
                    break;
                case 2:
                    PlayerXPos++;
                    break;
                case 3:
                    PlayerYPos--;
                    break;
                default:
                    PlayerXPos--;
                    break;
            }
        }



        public Monster CheckRandomEncounter()
        {
            int roll = RandomHelper.Random(0, 20);

            Monster monster = null;

            if (roll <= 5)
            {
                // No encounter, return nothing.
                return null;
            }
            else if (roll >= 6 && roll <= 10)
            {
                monster = new Monster("Orc", 10, 8, 200, 250, 1, "Short Sword", 2, 7);

                Console.WriteLine("You encountered an Orc!");
                Console.WriteLine("Prepare for battle!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine();
            }
            else if (roll >= 11 && roll <= 15)
            {
                monster = new Monster("Goblin", 6, 6, 100, 250, 0, "Dagger", 1, 5);

                Console.WriteLine("You encountered an Goblin!");
                Console.WriteLine("Prepare for battle!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine();
            }
            else if (roll >= 16 && roll <= 19)
            {
                monster = new Monster("Ogre", 20, 12, 500, 600, 2, "Club", 3, 8);

                Console.WriteLine("You encountered an Ogre!");
                Console.WriteLine("Prepare for battle!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine();
            }
            else if (roll == 20)
            {
                monster = new Monster("Orc Lord", 25, 15, 2000, 1000, 5, "Two Handed Sword", 5, 20);

                Console.WriteLine("You encountered an Orc Lord!!!");
                Console.WriteLine("Prepare for battle!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine();
            }
            return monster;
        }

        public void PrintPlayerPos()
        {
            Console.WriteLine("Player Position = (" + PlayerXPos.ToString() + ", " + PlayerYPos.ToString() + ")");
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
