﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Player
    {
        public string Name { get; private set; }

        // Constructor
        public Player()
        {
            Name = "Default";
            className = "Default";
            classRace = "Default";
            accuracy = 0;
            magicPoints = 
            hitPoints = 0;
            maxHitPoints = 0;
            expPoints = 0;
            nextLevelExp = 0;
            level = 0;
            Armor = 0;
            currentGold = 0;
            weapon = new Weapon("Default Weapon Name", 0, 0);
        }

        public void CreateClass()
        {
            Console.WriteLine("CHARACTER CREATION");
            Console.WriteLine("==========================");

            // Input character's name.
            System.Console.Write("Enter your character's name: ");
            //Name = Console.ReadLine();

            Console.WriteLine("\n1)Fighter     2)Wizard   3)Cleric    4)Thief ");
            System.Console.Write("Please select a Character Class...  : ");
            
            int characterNum = 1;

            //characterNum = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("\n1)Dwarf   2)Human   3)Elf    4)Halfling");
            System.Console.Write("Please select a Character Race...  : ");
            

            int characterRace = 1;
            characterRace = Convert.ToInt32(Console.ReadLine());


            switch (characterNum)
            {
                case 1: //Fighter
                    className = "Fighter";
                    accuracy = 10;
                    hitPoints = 20;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 4;
                    weapon = new Weapon("Long Sword", 1, 8);
                    break;
                case 2: //Wizard
                    className = "Wizard";
                    accuracy = 5;
                    hitPoints = 10;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 1;
                    weapon = new Weapon("Staff", 1, 4);
                    break;
                case 3:
                    className = "Cleric";
                    accuracy = 8;
                    hitPoints = 15;
                    maxHitPoints = 15;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 3;
                    weapon = new Weapon("Flail", 1, 6);
                    break;
                default: //
                    className = "Thief";
                    accuracy = 11;
                    hitPoints = 12;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 2;
                    
                    weapon = new Weapon("Dagger", 1, 6);
                    break;
            }


            switch (characterRace)
            {
                case 1: //Fighter
                    classRace = "Dwarven ";
                    accuracy -= 5;
                    hitPoints += 10;
                    maxHitPoints = hitPoints;
                    break;
                case 2: //Wizard
                    classRace = "Human ";
                    accuracy += 2;
                    hitPoints += 3;
                    maxHitPoints = hitPoints;
                    break;
                case 3:
                    classRace = "Elven ";
                    accuracy += 5;
                    hitPoints -= 3;
                    maxHitPoints = 15;
                    break;
                default: //
                    classRace = "Halfling ";
                    accuracy += 3;
                    hitPoints += 2;
                    maxHitPoints = hitPoints;
                    break;
            }
        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set; }
        public bool Attack(Monster monsterTarget)
        {
            int selection = 1;
            Console.Write("1) Attack, 2) Run");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:
                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        } else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;
                case 2:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
            }

            return false;
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp()
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points requard for next level
                nextLevelExp = level * level * 1000;

                // Increase stats randomly; old level up skills
                /*accuracy += RandomHelper.Random(1, 3);
                maxHitPoints += RandomHelper.Random(2, 6);
                Armor += RandomHelper.Random(1, 2);*/

                //New levelling system
                if (className == "Fighter")
                {
                    accuracy += 1;
                    maxHitPoints += 4;
                    Armor += 3;
                }
                else if(className == "Wizard")
                {
                    accuracy += 3;
                    maxHitPoints += 2;
                    Armor += 2;
                }
                else if (className == "Cleric")
                {
                    accuracy += 2;
                    maxHitPoints += 3;
                    Armor += 2;
                }
                else if (className == "Thief")
                {
                    accuracy += 4;
                    maxHitPoints += 1;
                    Armor += 2;
                }

                    // Give the player full hitpoints when they level up.
                    hitPoints = maxHitPoints;
            }
        }

        public int Rest()
        {
            Console.WriteLine("Resting...");

            hitPoints = maxHitPoints;
            // TODO: Modify the function so that random enemy enounters
            // are possible when resting
            int encounterRate = RandomHelper.Random(1, 4);

            if (encounterRate == 1)
            {
                //monster = gameMap ;
                return 1;
            }
            else return 0;
        }

        public void ViewStats()
        {
            Console.WriteLine("\nPLAYER STATS");
            Console.WriteLine("======================");
        
            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Type             = " + classRace + className);
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString() + " / " + maxHitPoints.ToString());
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString() + " / " + nextLevelExp.ToString());

            Console.WriteLine("\nArmor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());
            Console.WriteLine("Current Gold     = " + currentGold.ToString());

            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("=======================");
            Console.WriteLine();
        }
        
        public void Victory(int xp, int gold)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            expPoints += 1000;
            //expPoints += xp;
            Console.WriteLine("You get " + gold.ToString() + " gold!");
            currentGold += gold;
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }
        
        private string className;
        private string classRace;
        private int hitPoints;
        private int magicPoints;
        private int maxHitPoints;
        private int expPoints;
        private int nextLevelExp;
        private int level;
        private int accuracy;
        private int currentGold;
        private Weapon weapon;
    }
}
