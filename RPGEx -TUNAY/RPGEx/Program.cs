﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Map gameMap = new Map();
            Player mainPlayer = new Player();
            mainPlayer.CreateClass();

            // Begin adventure
            bool done = false;
            while (!done)
            {
                // Each loop cycle we output the player position
                // and a selection menu
                gameMap.PrintPlayerPos();

                int selection = 1;
                Console.Write("(1) Move      (2) Rest      3) View Stats       4) Quit       :");
                selection = Convert.ToInt32(Console.ReadLine());
                Monster monster = null;

                switch (selection)
                {
                    case 1:
                        gameMap.MovePlayer();

                        // Check for a random encounter. This function
                        // returns a null pointer if no monster are
                        // encoutered.
                        goto case 5;
                        
                    case 2:
                        int enemySeen = mainPlayer.Rest();
                        if (enemySeen == 1) goto case 5; 
                        break;
                    case 3:
                        mainPlayer.ViewStats();
                        break;
                    case 4:
                        done = true;
                        break;
                    case 5:
                        monster = gameMap.CheckRandomEncounter();

                        if (monster != null)
                        {
                            while (true)
                            {
                                // Display hitpoints
                                mainPlayer.DisplayHitPoints();
                                monster.DisplayHitPoints();
                                Console.WriteLine();

                                bool runAway = mainPlayer.Attack(monster);

                                if (runAway)
                                    break;

                                if (monster.isDead)
                                {
                                    mainPlayer.Victory(monster.ExpReward, monster.GoldReward);
                                    mainPlayer.LevelUp();
                                    break;
                                }

                                monster.Attack(mainPlayer);

                                if (mainPlayer.isDead)
                                {
                                    mainPlayer.GameOver();
                                    Console.WriteLine("---------------------------------------------");
                                    done = true;
                                    break;
                                }
                            }
                            if (gameMap.PlayerXPos == 1 && gameMap.PlayerYPos == 1)
                            {
                                 Store store = new Store();
                                //store.enter(mainPlayer);
                            }
                        }
                        break;
                }
            }
        }
    }
}
