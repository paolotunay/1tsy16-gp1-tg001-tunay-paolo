﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Spell
    {
        public string Name;
        public Range DamageRange;
        public int MagicPointsRequired;
        public int spelldmg;

        //Constructor
        public Spell(string name, int mpr, int dmg)
        {
            Name = name;
            //DamageRange = rng;
            MagicPointsRequired = mpr;
            spelldmg = dmg;
        }
    }


}
