﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public struct Range
    {
        public int Low;
        public int High;
    }
}
