﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize commanders and set turn order
            Queue<Commander> commanders = InitializePlayers();

            // Play the game until one commander's hand is empty.
            int round = 0;

            while (true)
            {
                // Print round
                Console.Clear();
                PrintRound(commanders, ++round);

                // Evaluate next commander's turn
                Commander activeCommander = commanders.Dequeue();
                Commander defendingCommander = commanders.Peek();
                activeCommander.EvaluateTurn(defendingCommander);

                // Put this commander back to the queue
                commanders.Enqueue(activeCommander);
                Console.WriteLine();

                // Check for empty hand.
                if (HasEmptyHand(commanders))
                {
                    Console.WriteLine("Game has ended. Evaluating points...");
                    foreach (Commander commander in commanders)
                    {
                        Console.WriteLine(commander.Name + " has " + commander.Cards.Count + " cards left.");
                        commander.OnGameEnding();
                    }

                    // Break out of the loop since game has ended
                    break;
                }

                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
                Console.Clear();
            }

            // Game has ended. Display results. This code assumes that there are only 2 players.
            ReportResults(commanders.Dequeue(), commanders.Dequeue());

            Console.Read();
        }

        static Queue<Commander> InitializePlayers()
        {
            // Initialize commanders
            Player player = new Player();
            player.Name = "Player";
            AddStartingCards(player);

            AI ai = new AI();
            ai.Name = "AI";
            AddStartingCards(ai);

            // Enqueue them (random order)
            Queue<Commander> commanders = new Queue<Commander>();
            bool playerFirst = RandomHelper.Chance(0.5f);
            if (playerFirst)
            {
                commanders.Enqueue(player);
                commanders.Enqueue(ai);
            }
            else
            {
                commanders.Enqueue(ai);
                commanders.Enqueue(player);
            }

            return commanders;
        }

        static void AddStartingCards(Commander commander)
        {
            while (commander.Cards.Count < 10)
            {
                // Pick random card
                int rand = RandomHelper.Range(3);
                switch (rand)
                {
                    case 0:
                        commander.Cards.Add(new Warrior());
                        break;
                    case 1:
                        commander.Cards.Add(new Mage());
                        break;
                    case 2:
                        commander.Cards.Add(new Assassin());
                        break;
                    default:
                        break;
                }
            }
        }

        static void PrintRound(IEnumerable<Commander> commanders, int round)
        {
            Console.WriteLine("===============================");
            Console.WriteLine("Round " + round.ToString());
            Console.WriteLine("Points");
            foreach (Commander commander in commanders)
            {
                Console.WriteLine(commander.Name + " : " + commander.Points);
            }
            Console.WriteLine("===============================");
        }

        static bool HasEmptyHand(IEnumerable<Commander> commanders)
        {
            foreach (Commander commander in commanders)
            {
                if (commander.EmptyHand) return true;
            }

            return false;
        }

        static void ReportResults(Commander a, Commander b)
        {
            // Print points
            Console.WriteLine(a.Name + " : " + a.Points);
            Console.WriteLine(b.Name + " : " + b.Points);
            Console.WriteLine();

            Commander winner = null;
            if (a.Points > b.Points) winner = a;
            else if (b.Points > a.Points) winner = b;

            // Winner
            if (winner != null)
            {
                Console.WriteLine(winner.Name + " wins!");
            }
            // Draw
            else
            {
                Console.WriteLine("Draw!");
            }
        }
    }
}
