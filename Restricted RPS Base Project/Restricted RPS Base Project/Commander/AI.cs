﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            if (RandomHelper.Chance(0.7f))
            {
                Console.WriteLine("AI challenged Player to a fight...");
                Fight(opponent);
            }
            else
            {
                Console.WriteLine("AI chooses to Discard on the round...");
                Cards.Add(Discard());
            }
        }

        public override Card PlayCard()
        {
            int AIchoice = RandomHelper.Range(Cards.Count);
            Card chosencard = Cards[AIchoice];
            Cards.RemoveAt(AIchoice);
            return chosencard; 
        }
    }
}
