﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            
            DisplayCards();
            Console.WriteLine("\nPress 1 to Play a card");
            Console.WriteLine("Press 2 to Discard");
            Console.Write("Press Q to Quit Console ");

            ConsoleKeyInfo turnChoice;
            while (true)
            {
                turnChoice = Console.ReadKey();
                if (turnChoice.Key == ConsoleKey.D1)
                {
                    Fight(opponent);
                    break;
                }
                if (turnChoice.Key == ConsoleKey.D2 && CanDiscard)
                {
                    Card newcard = Discard();
                    Cards.Add(newcard);
                    Console.WriteLine("\nTwo cards were discarded and a new card was drawn: " + newcard.Type);
                    break;
                }
                if (turnChoice.Key == ConsoleKey.Q) Environment.Exit(0);
            }
        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            int swA = 0, swM = 0, swW = 0;
            DisplayCards();
            foreach (Card cards in Cards)
            {
                if (cards.Type == "Assassin") swA = 1;
                else if (cards.Type == "Mage") swM = 1;
                else if (cards.Type == "Warrior") swW = 1;
            }

            if (swA == 1) Console.WriteLine("\n (1) ASSASSIN");
            if (swM == 1) Console.WriteLine("\n (2) MAGE");
            if (swW == 1) Console.WriteLine("\n (3) WARRIOR");

            Console.Write("\nChoose a card: ");
            int chosenCard = Convert.ToInt32(Console.ReadLine());
            string type = "default";

            if (chosenCard == 1)
            {
                type = "Assassin";
            }
            else if (chosenCard == 2)
            {
                type = "Mage";
            }
            else if (chosenCard == 3)
            {
                type = "Warrior";
            }

            foreach (Card cards in Cards)
            {
                if (type == cards.Type)
                {
                    Card duplicate = cards;
                    Cards.Remove(cards);
                    return duplicate;
                }
            }
            return null;
        }
    }
}
