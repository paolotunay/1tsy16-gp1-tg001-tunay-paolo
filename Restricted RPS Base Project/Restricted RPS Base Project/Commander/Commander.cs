﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        private List<Card> cards = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
            get
            {
                if (cards.Count > 1)
                {
                  return true;   
                }
                else return false;
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            int drawRand = RandomHelper.Range(3);
            if (drawRand == 0) return new Assassin();
            else if (drawRand == 1) return new Mage();
            else return new Warrior();
        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            if (!CanDiscard) throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE

            for (int x = 0; x < 2; x++)
            {
                int drawRand = RandomHelper.Range(cards.Count);
                cards.RemoveAt(drawRand);
            }
            return Draw();
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            Console.WriteLine("\n");
            Console.WriteLine("CURRENT HAND");
            for (int x = 0; x < cards.Count; x++) Console.WriteLine("  " + (x + 1) + ". " + Cards[x].Type);
        }

        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            Points -= cards.Count;
            //Cards.Clear();
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();

        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();

            Console.WriteLine("\n" + Name + " summons " + myCard.Type);
            Console.WriteLine(opponent.Name +" summons " + opponentCard.Type);

            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Points += 2;
                Console.WriteLine("\n" + myCard.Type + " wins the round!");
            }
            else if (result == FightResult.Draw)
            {
                Console.WriteLine("\nDraw!");
            }
            else 
            {
                Console.WriteLine("\n" + opponentCard.Type + " wins the round!");

            }
        }
    }
}
