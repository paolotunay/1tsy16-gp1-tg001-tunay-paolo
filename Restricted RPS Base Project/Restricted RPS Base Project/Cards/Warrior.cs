﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public Warrior()
        {
            Type = "Warrior";
        }

        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Assassin") return FightResult.Win;
            else if (opponentCard.Type == "Mage") return FightResult.Lose;
            else return FightResult.Draw;
        }
    }
}
