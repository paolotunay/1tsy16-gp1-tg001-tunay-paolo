﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public Assassin()
        {
            Type = "Assassin";
        }

        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Assassin") return FightResult.Draw;
            else if (opponentCard.Type == "Mage") return FightResult.Win;
            else return FightResult.Lose;
        }
    }
}
