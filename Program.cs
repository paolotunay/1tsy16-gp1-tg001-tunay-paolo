﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Act4GMPROG919
{
    public class CircleProperties
    {
        public int id;
        private double radius = 1;
        private string color = "red";

        public double Radius
        {
            get
            {
                return radius;
            }

            set
            {
                radius = value;
            }
        }

        public string Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            CircleProperties Circle1 = new CircleProperties();

            Console.WriteLine("\n\n Current Radius: " + Circle1.Radius);
            Console.WriteLine(" Current Color: " + Circle1.Color);

            System.Console.Write("\n Set New Radius Value: ");
            int inputradius = Convert.ToInt32(Console.ReadLine());
            Circle1.Radius = inputradius;

            System.Console.Write(" Set New  Color: ");
            string inputcolor = Console.ReadLine();
            Circle1.Color = inputcolor;

            Console.WriteLine("\n\n The New Radius: " + Circle1.Radius);
            Console.WriteLine(" The New Color: " + Circle1.Color);


            Console.ReadLine();
        }
    }
}
