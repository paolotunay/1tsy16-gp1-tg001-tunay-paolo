﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResistanceBase
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            if (totalPlayers < 7) return 2; // 5-6 players = 2 spies
            else if (totalPlayers > 8) return 4; // 9-10 players = 4 spies
            else return 3; // 7-8 players = 3 spies
        }

        static int EvaluateRound(string[] players)
        {    
            for (int x = 0; x < players.Length; x++)
            {
                if (players[x] == "Resistance") Console.WriteLine("- Player " + (x+1) + " is part of the Resistance. Auto Success!");
                else { System.Console.WriteLine("- Player " + (x+1) + " is a Spy. Choose S/F: ");

                    while (true)
                    {
                        string Spyturn = Console.ReadLine();
                        if (Spyturn == "S") break;
                        else if (Spyturn == "F")  return 1; // Return 1 if spies win 
                    }
                }
            }
            Console.WriteLine("Mission Success! Press any key to continue... "); Console.ReadKey();
            return 0;     // Return 0 if resistance wins
        }


        static void PrintPlayers(string[] players)
        {
            foreach (string roleassigned in players)
            {
                Console.WriteLine(roleassigned);
            }
        }

        static void AssignRoles(string[] players)
        {
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more

           int spies = GetNumSpies(players.Length); // Must be dependent on the number of spies

           for(int x = 0; x < players.Length ; x++)
            {
                if (players[x] != "Resistance" && spies > 0) players[x] = "Spy"; 
                else players[x] = "Resistance";
                spies--; 
            }

            PrintPlayers(players);
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            if (totalPlayers < 7) return 3; // 5-6 players = 3 per mission
            else if (totalPlayers > 8) return 5; // 9-10 players = 5 per mission
            else return 4; // 7-8 players = 4 per mission
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Resistance!");

            int playersNum = 0;
            /////////////////////
            // QUIZ ITEM: Loop until the player enters a valid number
            /////////////////////

            while (playersNum < 5 || playersNum > 10)
            {
                Console.Write("Enter number of players (5-10)");
                playersNum = Convert.ToInt32(Console.ReadLine());
            }
            // END LOOP

            // Make an array of players based on the number the player entered
            string[] players = new string[playersNum];
            // Assign roles to the players
            AssignRoles(players);
            Console.Read();

            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());
                Console.WriteLine();
                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players

                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions

            if (resistanceScore >= 3)
            {
                Console.WriteLine("\n::: Game Over. Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("\n::: Game Over. Spies Win!");
            }
            Console.ReadKey();

        }
    }
}
